var SurveyMonkeyAPI = require('./surveymonkey/index').SurveyMonkeyAPI;
var fs = require('fs');
var jsonfile = require("jsonfile");


// var JSON_HOME = "./survey_json/";
// var accessToken = 'tgnhrpXfWqAq7LiQXBjI-K-DcLcRt7XF8bNY6XLY9oNNjNPw6LdA1dtRDKCqJu9NlJnLg9zc905FQ6YgfKx.aTvPlWnal8kKbWUnRBHfYozwS2EAXoNMw4NRZvX.0ZBz';
var surveryParser = "http://34.208.110.177:8000/webhook";

//api is the gateway for us to access data in surveyMonkey
// try {
//     var api = new SurveyMonkeyAPI(accessToken, { version : 'v3', secure : false });
//     //
// } catch (error) {
//     console.log(error.message);
// }

// var DATA = './ChrisBio.json';
// var QUESTION = './StudentBio.json';
//
//
// var STU_BIO_DIC = './StuBioDictionary.json';
// var STAFF_BIO_DIC = './StaffBioDictionary.json';
//
// var STU = /^Student$/;
// var STAFF = /^Admin|DOE|PMC|FT$/;


/**
  public accessable function. basicly, most of the functions in this section are utility functions.
  1. They should never have the ability to update db or chaneg server status
  2. public accessable @public
*/
var titleParse = function(str) {
  var DEFAULT_REPLACE = /(\s)|(\/)/gi;
  if (!str || typeof(str) != 'string') {
    return null;
  }
  return str.replace(DEFAULT_REPLACE,'_');
}

var dfsQueJson = function(json,result) {
  //gnerate key-val pair of question--questions_id
  if (!json) {
    return;
  } else if (json.headings) {
    result[json.id] = json.headings[0]['heading'];
  } else if (json.text) {
    result[json.id] = json.text;
    return;
  } else if (json.title) {
    result[json.id] = json.title;
  }
  for (var i in json) {
    if (typeof(json[i]) == 'object') {
      dfsQueJson(json[i],result);
    }
  }
}

var dfsAnsJson = function(ans,question,dic,output) {
  //generate key-value pair for question and ans
  if (!ans) {
    return;
  } else if (ans.answers) {
    dealAnswerandID(ans,question,question[ans.id],dic,output);
    return;
  }
  for (var i in ans) {
    if (typeof(ans[i]) == 'object') {
      dfsAnsJson(ans[i],question,dic,output);
    }
  }
}

var dealAnswerandID = function(ans,question,header,dic,output) {
  console.log(ans);
  if (typeof(ans) != 'object') {
    return;
  }
  if (ans.choice_id || ans.row_id || ans.text) {
    var key = header;
    var val = "";
    if (ans.choice_id && !ans.row_id && ! ans.text) {
      val += question[ans.choice_id];
    } else if (ans.choice_id && (ans.row_id || ans.text)) {
      key += question[ans.choice_id];
      if (ans.row_id) {
        val += question[ans.row_id];
      }
      if (ans.text) {
        val += ans.text;
      }
    } else if (ans.row_id && ans.text) {
      key += question[ans.row_id];
      val += ans.text;
    } else if (ans.row && !ans.text) {
      val += question[ans.row_id];
    } else {
      val += ans.text;
    }
    console.log(dic[key],'   ',val);
    if (!output[dic[key]]) {
      output[dic[key]] = val;
    } else if (typeof(output) == 'string') {
      output[dic[key]] = [output[dic[key]],val];
    } else {
      output[dic[key]].push(val);
    }
    return;
  } else {
    for (var cell in ans) {
      if (typeof(ans[cell]) == 'object') {
        dealAnswerandID(ans[cell],question,header,dic,output);
      }
    }
  }

}
//////////////////////////////////////////////////////////////end of public function/////////////////////////////////////////////////////////


/**
  Parser Controller did initialize Work, It will call a lot function for the first time, to make sure dictionary for all
  surveys are ready;
*/
function ParserContorller (token,json_home,dic_home) {
  this.accessToken = token;
  this.surveryParser = {};
  this.stu = /^Student$/;
  this.staff = /^Admin|DOE|PMC|FT$/;
  this.JSON_HOME = json_home;
  this.DIC_HOME = dic_home;
  this.SurveyMap = {
    'New_Employee_Form':'staffInfo',
    'Student_Application_#1:_Biographical_Information':'studentInfo',
    'webhook':'webhook',
    'Harrison_Test_Copy':'studentInfo',
    'test_Bio':'studentInfo'
  };
  // this.api = null;
  try {
    this.api = new SurveyMonkeyAPI(this.accessToken, { version : 'v3', secure : false });
    console.log('success :',this.api);
  } catch (error) {
    console.log(error.message);
  }

  //////////////////private functions
  /**
  this should be a private variable, Please checnk if this function is only accessble inside this controller;
  @private please check
  */
  this.writeSurveyJson = function(name,json) {
    // jsonfile.spaces = 4;
    var path = this.JSON_HOME + name +'.json';
    // console.log(path);
    var question = {};
    dfsQueJson(json,question);
    // console.log
    jsonfile.writeFile(path,question,function(err) {
        if (err) {
          console.log(err);
        }
      });
    }

  /**
    When user start doing survey, we send a request to surveyMonkey to establish a webhook between user and surveyMonkey
    @see https://developer.surveymonkey.com/api/v3/#webhooks
    @param input should have _id/ip/surveyName
  */
  this.setCollectorWebhook = function (input) {
    if (!input || !input.title) {
      console.log(FORMAT_ERR);
      return;
    }
    var params = {
      'name':input.title,
      "event_type":"response_completed",
      "object_type":"collector",
      "object_ids":input.ids,
      "subscription_url":surveryParser + '/' + input.id
    };
    console.log('create webhook: ',params);
    this.api.createWebHook(params,function(err){
      if (err) {
        console.log(err);
        return;
      }
      console.log('survey_id : '.input.id,'\n   collectors:',input.ids,'webhook create success');
    });
  }

  this.setSurveyWebhook = function(input,array,index) {
    if (!input || !input.title) {
      console.log(FORMAT_ERR);
      return;
    }
    var params = {
      'name':input.title,
      "event_type":"response_completed",
      "object_type":"survey",
      "object_ids":input.ids,
      "subscription_url":surveryParser + '/' + input.id + (new Date().getTime())
    };
    var that = this;
    this.api.createWebHook(params,function(err){
      if (err) {
        console.log(err);
        return;
      }
      console.log('survey_id : ',input.id,'   ',input.ids,'  webhook create success');
      if (index < array.length) {
        var nextinput = {'id':array[index]['id'],'title':titleParse(array[index]['title']),'ids':[array[index]['id']]};
        that.setWebHooksForSurveys(nextinput,array,index + 1);
      }
    });
  }

  this.setWebHooksForCollectors = function (id,map,title) {
    var that = this;
    this.api.getCollectorList({'id':id},function(error,data) {
      if (error) {
        console.log(error);
      } else {
        // console.log(data);
        if (data.total >= 1) {
          data = data.data;
          console.log(data);
          // var map = {};
          for (var i in data) {
            map[title].push(data[i].id);
            // that.collectorSurveyMap[data[i].id] = title;
          }
          var input = {'title':title,'id':id,'ids':map[title]};
          console.log(input);
          that.setCollectorWebhook(input);
        }
      }
    });
  };

  this.setWebHooksForSurveys = function (input,array,index) {
    // var input = {'id':id,'title':title,'ids':[id]};
    console.log(input,index);
    this.setSurveyWebhook(input,array,index);
  }
  this.removeWebHook = function(id) {
    // console.log(this.api);
    // console.log(api,id);
    this.api.deleteWebHook({'id':id},function(err,del){
      if (err) {
        console.log(err);
      }
    });
  }

  this.hello = function() {
    console.log('I am controller, Hello world!');
    // console.log(this);
  }

};


module.exports.ParserContorller = ParserContorller;

//public functions
/**
  use api to retrieve details of survrys from SurveyMonkey & store them on the filesystem
  1. use getSurveyList -> array of surveys id
  2. iterate id array, and use getSurveyDetails({'id':id}) retrieve survey detail from SurveyMonkey
  3. use callback function to store the survey detail data on server

  consideration: since the survey may not be change so often. It is reasonable to keep copy of survey
  on server, which can save unnecessary API calls and decrease time consumption
*/

ParserContorller.prototype.initial = function () {
  //remove all webhooks which are existed
  // console.log(this);
  var that = this;
  // this.api.getWebHooks({},function(err,data){
  //   if (err) {
  //     console.log(err);
  //   } else {
  //     data = data['data'];
  //     console.log(data);
  //     // for (var i in data) {
  //     //   // console.log(this);
  //     //   that.removeWebHook(data[i]['id']);
  //     // }
  //   }
  // });

  // set webhooks and get json dic for eachwjmwjm wjm survey
  this.api.getSurveyList({}, function (error, data) {
    if (error) {
      console.log(error);
      return;
    }
    data = data.data;
    // console.log(this);
    // var map = {};
    for (var i in data) {
      // var id = data[i].id;
      // var title = data[i].title;
      var input = {'id':data[i].id,'title':titleParse(data[i].title),'ids':[data[i]['id']]};
      that.setWebHooksForSurveys(input,data,1);
    //   map[titleParse(data[i].title)] = [];
    //   that.setWebHooksForCollectors(data[i].id,map,titleParse(data[i].title));
      // that.api.getSurveyDetails({id:data[i].id},function (error, data) {
      //     if (error) {
      //         console.log(error);
      //         return;
      //     }
      //     that.writeSurveyJson(titleParse(data.title),data);
      // });
      break;
    }
  });

  //also create surveryParser for each of the survey we have

}

ParserContorller.prototype.initialSurveryParsers = function (connection) {
  for (var i in this.SurveyMap) {
    // console.log(this.SurveyMap[i]);
    this.surveryParser[i] = new SurveryParser(connection[this.SurveyMap[i]],this.DIC_HOME + this.SurveyMap[i] +'.json',this.JSON_HOME + i + '.json');
    // this.surveryParser[i].hello();
  }
  // console.log(this.surveryParser);
}

ParserContorller.prototype.dealResponse = function (who,surveyname,c_id,r_id) {
  // console.log('Hello from public function');
  console.log(c_id,r_id);
  var that = this;
  this.api.getColletorResponse({'c_id':c_id,'id':r_id},function(err,response){
    if (err) {
      console.log("delete webhook fail");
      return;
    }
    var input = {'_id':who[response['ip_address']],'ans':response['pages']};
    console.log(input);
    // console.log(JSON.stringify(response,undefined,4));

    if(that.surveryParser[surveyname]) that.surveryParser[surveyname].write(input);
  });
}

ParserContorller.prototype.helloworld = function () {
  this.hello();
};
///////////following functions maybe private, please think about it!///////


ParserContorller.prototype.getSuveryDictionaryList = function () {
  api.getSurveyList({}, function (error, data) {
    if (error) {
      console.log(error);
      return;
    }
    // console.log(data);
    data = data.data;
    for (var i in data) {
      api.getSurveyDetails({id:data[i].id},function (error, data) {
          if (error) {
              console.log(error);
              return;
          }
          this.writeJson(titleParse(data.title),data);
      });
    }
  });
}

ParserContorller.prototype.CreateAllWebHooks = function () {
  this.api.getSurveyList({}, function (error, data) {
      if (error) {
          console.log(error.message);
          return;
      }
      data = data.data;
      for (var i in data) {
        getCollectorList(data[i].id,titleParse(data[i].title));
      }
  });
}

ParserContorller.prototype.removeAllWebHooks = function() {
    this.api.getWebHooks({},function(err,data){
      if (err) {
        console.log(err);
      } else {
        data = data['data'];
        for (var i in data) {
          removeWebHook(data[i]['id']);
        }
      }
    });
}






/**
  in the future, for each survey, we gonna have their own parsedResponse
  @param connection should be a db collection
  @param dictionary should be a string indicate dictionary needs to parse the input
*/
var SurveryParser = function(connection,dictionary,question) {
  this.dic_path = dictionary;
  this.db = connection;
  this.ques_path = question;
  //
  // this.hello = function() {
  //   console.log('hello from surveyParser :',this.dic_path);
  // }
};

// module.exports.SurveryParser = SurveryParser;

SurveryParser.prototype.write = function (input) {
  /**
  @param
    input = {
    '_id':'mdsagfdgy121Xgdwhe/qwe!',
    'question':Object,
    'ans':Object
  }
  */
  if (!input || !input._id || !input.ans) {
    return false;
  }
  var output = {};
  var that = this;
  // var question = {};
  // dfsQueJson(input.question,question);
  jsonfile.readFile(this.ques_path,function(err,question){
    if (err) {
      console.log(err);
      return;
    }
    console.log('question:',question);
    jsonfile.readFile(that.dic_path,function(err,dic) {
        if (err) {
          console.log(err);
          return false;
        }
        // console.log('dic:----->:',dic);
        // console.log(input.ans);
        dfsAnsJson(input.ans,question,dic,output);

        //TODO: replace with postgresql insert function
        output['_id'] = input._id;
        console.log(output);
        that.db.insert(output,function(err) {
          if (err) {
            console.log(err);
            return;
          }
          console.log('insert success');
        });
        // that.db.update(output,{upsert:true},function(err) {
        //   if (err) {
        //     console.log(err);
        //     return false;
        //   }
        //   console.log('update success');
        //   return true;
        // });
    });
  });

}

SurveryParser.prototype.writeDB = function (input) {
  //operation on database is at most one operation
  // if the insert fail, we will lose the opportunity to insert information;
  // maybe we should make it a at least one operation
  // science the operation on database will only keep one record of the input
  /**
  @param
    input = {
    'role':'Student',
    '_id':'mdsagfdgy121Xgdwhe/qwe!',
    'question':Object,
    'ans':Object
  }
  */
  if (!input) {
    return;
  }
  var role = input.role;
  if (!role ||  !input._id) {
    return;
  } else if (role.match(STU)) {
    var output = {};
    var question = {};
    dfsQueJson(input.question,question);
    jsonfile.readFile(STU_DIC,function(err,dic) {
        if (err) {
          console.log(err);
        } else {
          dfsAnsJson(input.ans,question,dic,output);
            //TODO: replace with postgresql insert function
          output['_id'] = input._id;
          connections['studentInfo'].update(output,{upsert:true},function(err) {
            if (err) {
              console.log(err);
              return;
            }
          });
        }
    });
  } else if (role.match(STAFF)) {
    var output = {};
    var question = {};
    dfsQueJson(input.question,question);
    jsonfile.readFile(STAFF_DIC,function(err,dic) {
        if (err) {
          console.log(err);
        } else {
          dfsAnsJson(input.ans,question,dic,output);
          //TODO: replace with postgresql insert function
          output['_id'] = input._id;
          connections['staffInfo'].update(output,{upsert:true},function(err) {
            if (err) {
              console.log(err);
              return;
            }
          });
        }
    });
  }
}

SurveryParser.prototype.hello = function () {
  console.log('hello from surveyParser :',this.dic_path);
  console.log(this);
}

// surveryParser.prototype.fetch = function () {
//
// }

// jsonfile.readFile(QUESTION,function(err,que){
//   if (err) {
//     console.log(err);
//   } else {
//     // console.log(ans);
//     var question = {};
//     dfsQueJson(que,question);
//     jsonfile.readFile(DATA,function(err,ans){
//       if (err) {
//         console.log(err);
//       } else {
//         // console.log('\n');
//         // console.log(que);
//         jsonfile.readFile(STU_DIC,function(err,dic){
//           if (err) {
//             console.log(err);
//           } else {
//             dfsAnsJson(ans,question,dic);
//           }
//         });
//       }
//     });
//   }
// });

// test('  ',0);
// test('',0);
//
// var test = function(divide,n) {
//   if (n >= 100) {
//     console.log(divide,n);
//   } else {
//     console.log(divide,n);
//     test(divide,n + 1);
//   }
// }
// dfsJson(sample);
