var express = require('express');
var router = express.Router();
var SurveyMonkeyAPI = require('./surveymonkey/index').SurveyMonkeyAPI;
var jsonfile = require("jsonfile");
var yaml = require('js-yaml');
var fs = require('fs');
var path = require('path');
var util = require('../utility');
var db = require('../query');
var idTitle = {};
var titleId = {};
const config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '../config.yaml'), 'utf8'));


var os = require('os');
var IPv4,hostName;
hostName=os.hostname();
console.log(os.networkInterfaces());
// os.networkInterfaces().en0.forEach((val,index,arr)=>{
//     // console.log(val);
//     if (val.family === 'IPv4') {
//         console.log('find IPv4');
//         IPv4 = val.address;
//     }
// })
// console.log('----------local IP: '+IPv4);
// console.log('----------local host: '+hostName);


var accessToken = config["ACCESSTOKEN"];
var surveyPath = config["SURVEYDETAIL_PATH"];
var surveyParser = config["PARSERURL"];
//establish connection with surveymonkey server
try {
    var api = new SurveyMonkeyAPI(accessToken, { version : 'v3', secure : false });
    console.log('api created');
    //
} catch (error) {
    console.log(error.message);
}



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


/**generate id:string map for each survey in surveyMonkey
 * output: json file in path '../data/survey/'
 * */
router.get('/QuesIdStringMap',function (req,res,next) {
    api.getSurveyList({},function (err,data) {
        if (err) {
          console.log(err);
          return next(err);
        }
        data = data.data;
        data.forEach(d => {
            api.getSurveyDetails({id:d['id']},function (err,result) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
                let filerInfo = {};
                util.parseQues(result,filerInfo);
                jsonfile.writeFile(surveyPath + util.parseTitle(result.title),filerInfo,{spaces:config.SPACES},function (err) {
                    if (err)
                        console.log(err);
                });
            });
        });
        res.json(data);
    });
})

router.head('/parse/:id',function(req,res,next) {
    console.log('webhook head request',req.params);
    res.end('success');
});

router.get('/parse/:id',function (req,res,next) {
    if (!req.body.resources || !req.body.resources.collector_id || !req.body.resources.respondent_id) {
        return next(new Error('webhook return wrong format'));
    }

    let s_id = req.params.id;
    let s_title = idTitle[s_id];

    api.getColletorResponse({'c_id':req.body.resources.collector_id,'id':req.body.resources.respondent_id},function(err,response){
        if (err) {
            console.log(err);
            return next(err);
        }
        console.log(response);
        jsonfile.readFile(path.join(__dirname, ('../data/survey/'+s_title)),function(err,question){
            if (err) {
                console.log(err);
                return next(err);
            }
            jsonfile.readFile(path.join(__dirname, ('../data/quesDic/'+s_title)),function(err,dic) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
                let output = {};
                util.parseAns(response,question,dic,output);
                //todo: we need to tell which table the output will store in
                db.insertStudent(output);
                res.json(output);
                //todo: we need to insert this json into db instead of sending it back to client

            });
        });
    });

    // jsonfile.readFile(path.join(__dirname, '../data/survey/Student_Application_#1:_Biographical_Information'),function(err,question){
    //     if (err) {
    //         console.log(err);
    //         return next(err);
    //     }
    //     jsonfile.readFile(path.join(__dirname, '../data/quesDic/sample.json'),function(err,dic) {
    //         if (err) {
    //             console.log(err);
    //             return next(err);
    //         }
    //         jsonfile.readFile(path.join(__dirname, '../data/response/sampleAns.json'),function (err,ans) {
    //             if (err) {
    //                 console.log(err);
    //                 return next(err);
    //             }
    //             var output = {};
    //             util.parseAns(ans,question,dic,output);
    //             res.json(output);
    //             db.insertStudent(output);
    //         })
    //     });
    // });
});

router.get('/webhooks',function (req,res,next) {
    api.getWebHooks({},function (err,data) {
        if (err) {
            console.log(err);
            return next(err);
        }
        res.json(data);
    });
})

/**delete all webhooks on surveyMonkey server
 * */
router.get('/deleteAllWebhooks',function (req,res,next) {
    api.getWebHooks({},function(err,data){
        if (err) {
            console.log(err);
            return next(err);
        } else {
            data = data['data'];

            data.forEach(d => {
                console.log(d);
                api.deleteWebHook({'id':d.id},function(err,del){
                    if (err) {
                        console.log(err);
                        return next(err);
                    }

                });
            });
            res.json(data);
        }
    });
})

/**create webhooks based on survey list
 * if webhook for a survey is existed already, skip set webhooks
 * */
router.get('/initWebhooks',function (req,res,next) {
    api.getWebHooks({},function (err,hlist) {
        if (err) {
            console.log(err);
            return next(err);
        }
        hlist = hlist.data;
        // console.log('webhook list:',hlist);
        let hName = {};
        hlist.forEach(l => {
            hName[l.name] = true;
        });
        let result = [];
        for (var t in titleId) {
            if (!hName[t]) {
                result.push({
                    'title':t,
                    'ids':[titleId[t]],
                    'id':titleId[t]
                });
            }
        }

        console.log('need create webhook for survey:',result);
        if (result.length > 0)
            setSurveyWebhook(result[0],result,1);
        res.json(hlist);
    })
})

/**create one webhook for a survey specified by id
 * */
var setSurveyWebhook = function(input,array,index) {
    if (!input || !input.title) {
        console.log('wrong format');
        return;
    }
    // console.log(input);
    var params = {
        'name':input.title,
        "event_type":"response_completed",
        "object_type":"survey",
        "object_ids":input.ids,
        "subscription_url": surveyParser + input.id
    };
    console.log(params);
    api.createWebHook(params,function(err){
        if (err) {
            console.log(err);
            return;
        }
        // console.log('webhook create success!',array);
        if (index < array.length)
            setSurveyWebhook(array[index],array,index + 1);
    });
}


var initial = function () {
    api.getSurveyList({},function (err,list) {
        // console.log('initial parser -- survey List:',list);
        if (err) {
            console.log(err);
            return next(err);
        }
        list = list.data;
        list.forEach(l => {
            let title = util.parseTitle(l.title);
            idTitle[l.id] = title;
            titleId[title] = l.id;
        });
    });
}

initial();

/**get local ip address
 * */

module.exports = router;


