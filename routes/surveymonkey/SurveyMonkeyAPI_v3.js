var http = require('http'),
	request = require('request'),
	helpers = require('./helpers');

/**
 * SurveyMonkey API wrapper for the API version 1.3. This object should not be
 * instantiated directly but by using the version wrapper {@link SurveyMonkeyAPI}.
 *
 * @param apiKey The API key to access the SurveyMonkey API with
 * @param options Configuration options
 * @return Instance of {@link SurveyMonkeyAPI_v1_3}
 */
function SurveyMonkeyAPI_v3 (accessToken, options) {

	if (!options) {
		options = {};
	}

	this.version     = 'v3';
	this.accessToken = accessToken;
	this.secure      = options.secure || false;
	this.packageInfo = options.packageInfo;
	this.httpUri     = (this.secure) ? 'https://api.surveymonkey.net' : 'https://api.surveymonkey.net';
	this.userAgent   = options.userAgent+' ' || '';

}

module.exports = SurveyMonkeyAPI_v3;

/**
 * Sends a given request as a JSON object to the SurveyMonkey API and finally
 * calls the given callback function with the resulting JSON object. This
 * method should not be called directly but will be used internally by all API
 * methods defined.
 *
 * @param resource SurveyMonkey API resource to call
 * @param method SurveyMonkey API method to call
 * @param params Parameters to call the SurveyMonkey API with
 * @param callback Callback function to call on success
 */
SurveyMonkeyAPI_v3.prototype.execute = function (type, resource, method, params, callback) {

	var uri = {
		'responses_bulk' : this.httpUri + '/' + this.version + '/' + resource + '/' + params.id + '/responses/bulk',
		'responses' : this.httpUri + '/' + this.version + '/' + resource + '/' + params.id + '/' + method,
		'get_responses' : this.httpUri + '/' + this.version + '/' + resource + '/' + params.id,
		'get_survey_details' : this.httpUri + '/' + this.version + '/' + resource + '/' + params.id + '/details',
		'get_survey_list' : this.httpUri + '/' + this.version + '/' + resource,
		'get_collector_list' : this.httpUri + '/' + this.version + '/' + resource + '/' + params.id + '/collectors',
		'get_response_counts' : this.httpUri + '/' + this.version + '/' + resource + '/' + params.id + '/responses',
		'get_user_details' : this.httpUri + '/' + this.version + '/' + resource + '/me',
		'get_webhookes' : this.httpUri + '/' + this.version + '/' +resource,
		'get_webhook' : this.httpUri + '/' + this.version + '/' +resource + '/' + params.id,
		'get_collector_response' : this.httpUri + '/' + this.version + '/collectors/' + params.c_id + '/responses/' + params.id + '/details'
	};

	function serialize(obj) {
  	return '?' + Object.keys(obj).map(k => k + '=' + encodeURIComponent(obj[k])).join('&');
	}

	var queryParams = Object.assign({}, params);
	delete queryParams.id;
	if (queryParams.c_id) delete queryParams.c_id;
	request({
		uri : uri[method] + serialize(queryParams),
		method: type,
		//headers : { 'User-Agent' : this.userAgent+'node-surveymonkey/'+this.packageInfo.version },
		headers : { 'Authorization' : 'bearer ' + this.accessToken,
		'Content-Type' : 'application/json' }
	}, function (error, response, body) {
		var parsedResponse;
		if (error) {
			callback(new Error('Unable to connect to the SurveyMonkey API endpoint.'));
		} else {

			try {
				parsedResponse = JSON.parse(body);
			} catch (error) {
				callback(new Error('Error parsing JSON answer from SurveyMonkey API.'));
				return;
			}

			if (parsedResponse.errmsg) {
				callback(helpers.createSurveyMonkeyError(parsedResponse.errmsg, parsedResponse.status));
				return;
			}

			callback(null, parsedResponse);
		}
	});
};




/**
	Send given json to SurveyMonkey Survey as an obj and finally calls the given callback function
	with result data;
	This method should never been called directly
*/
SurveyMonkeyAPI_v3.prototype.executePost = function(type,method,param,callback) {
	var url  = {
		'create_webhook' : this.httpUri + '/' + this.version + '/' + 'webhooks',
		'delete_webhook' : this.httpUri + '/' + this.version + '/' + 'webhooks/'+ param.id
	}
	// console.log(url[type],JSON.stringify(param));
	request({
		uri:url[type],
		method:method,
		body:JSON.stringify(param),
		headers : { 'Authorization' : 'bearer ' + this.accessToken,
		'Content-Type' : 'application/json' }
	},function(error,response,body) {
		// console.log('received:',body);
		var parsedResponse;
		if (error) {
			callback(new Error('Unable to connect to the SurveyMonkey API endpoint.'));
		} else {
			//
			// try {
			// 	parsedResponse = JSON.parse(body);
			// } catch (error) {
			// 	callback(new Error('Error parsing JSON answer from SurveyMonkey API.'));
			// 	return;
			// }
			//
			// if (parsedResponse.errmsg) {
			// 	callback(helpers.createSurveyMonkeyError(parsedResponse.errmsg, parsedResponse.status));
			// 	return;
			// }

			callback(null, parsedResponse);
		}
	});
}

/*****************************************************************************/
/************************* Survey Related Methods **************************/
/*****************************************************************************/

/**
 * Retrieves a paged list of respondents for a given survey and optionally collector bulk
 *
 *
 */
SurveyMonkeyAPI_v3.prototype.getRespondentListBulk = function (params, callback) {
	if (typeof params === 'function') callback = params, params = {};
	this.execute('GET', 'surveys', 'responses_bulk', params, callback);
};

/**
 * Retrieves a paged list of respondents for a given survey and optionally collector
 *
 * @see https://developer.surveymonkey.com/mashery/get_respondent_list
 */
SurveyMonkeyAPI_v3.prototype.getRespondentList = function (params, callback) {
	if (typeof params === 'function') callback = params, params = {};
	this.execute('GET', 'surveys', 'responses', params, callback);
};

/**
 * Takes a list of respondent ids and returns the responses that correlate to them. To be used with 'get_survey_details'
 *
 * @see https://developer.surveymonkey.com/mashery/get_responses
 */
SurveyMonkeyAPI_v3.prototype.getResponses = function (params, callback) {
	if (typeof params === 'function') callback = params, params = {};
	this.execute('GET','responses', 'get_responses',  params, callback);
};

/**
 * Retrieve a given survey's metadata.
 *
 * @see https://developer.surveymonkey.com/mashery/get_survey_details
 */
SurveyMonkeyAPI_v3.prototype.getSurveyDetails = function (params, callback) {
	if (typeof params === 'function') callback = params, params = {};
	this.execute('GET', 'surveys', 'get_survey_details', params, callback);
};

/**
 * Retrieves a paged list of surveys in a user's account.
 *
 * @see https://developer.surveymonkey.com/mashery/get_survey_list
 */
SurveyMonkeyAPI_v3.prototype.getSurveyList = function (params, callback) {
	if (typeof params === 'function') callback = params, params = {};
	this.execute('GET' ,'surveys', 'get_survey_list', params, callback);
};

/**
 * Retrieves a paged list of surveys in a user's account.
 *
 * @see https://developer.surveymonkey.com/mashery/get_collector_list
 */
SurveyMonkeyAPI_v3.prototype.getCollectorList = function (params, callback) {
	if (typeof params === 'function') callback = params, params = {};
	this.execute('GET', 'surveys', 'get_collector_list', params, callback);
};

/**
 * Returns how many respondents have started and/or completed the survey for the given collector.
 *
 * @see https://developer.surveymonkey.com/mashery/get_response_counts
 */
SurveyMonkeyAPI_v3.prototype.getResponseCounts = function (params, callback) {
	if (typeof params === 'function') callback = params, params = {};
	this.execute('GET', 'collectors', 'get_response_counts', params, callback);
};

/**
 * Returns basic information about the logged-in user.
 *
 * @see https://developer.surveymonkey.com/mashery/get_user_details
 */
SurveyMonkeyAPI_v3.prototype.getUserDetails = function (params, callback) {
	if (typeof params === 'function') callback = params, params = {};
	this.execute('GET', 'users', 'get_user_details', params, callback);
};
//



/**
	get response detail from collectors
*/
SurveyMonkeyAPI_v3.prototype.getColletorResponse = function(params,callback) {
	if (typeof(params) != 'object') {
		console.log("Response get fail");
		return;
	}
	console.log(params);
	this.execute('GET','','get_collector_response',params,callback);
}

/**
	Returns list of webhooks.
*/
SurveyMonkeyAPI_v3.prototype.getWebHooks = function(params,callback) {
	if (typeof params == 'function') callback = params, params = {};
	this.execute('GET','webhooks','get_webhookes', params,callback);
}


/**
	*Create WebHooks with specific subscription_url;
	*param is the data send to SurveyMonkey survey,this val is required
	*@see https://developer.surveymonkey.com/api/v3/#webhooks
*/
SurveyMonkeyAPI_v3.prototype.createWebHook = function(param,callback) {
	if (typeof(param) != 'object') {
		console.log('WEBHOOK CREATION FAIL');
		return;
	}
	this.executePost('create_webhook','POST',param,callback);
}


/**
	Get webhooks info
	params must contains id field
*/
SurveyMonkeyAPI_v3.prototype.getWebHook = function(param,callback) {
	if (typeof(param) != 'object' || !param.id) {
		console.log('WEBHOOK GET FAIL');
		return;
	}
	this.execute('GET','webhooks','get_webhook',param,callback);
}


/**
	Delete webhook from server
	params must contains id field
*/
SurveyMonkeyAPI_v3.prototype.deleteWebHook = function(param,callback) {
	if (typeof(param) != 'object') {
		console.log('WEBHOOK DELETE FAIL');
		return;
	}
	this.execute('DELETE','webhooks','get_webhook',param,callback);
	// this.executePost('delete_webhook','DELETE',param,callback);
}
