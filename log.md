# SurveyMonkey Parser Develop Log

## 1. Retrieve Survey Information
    goal: generate question_id:key json file. this file will be used to gnerate json data we need

    * retrieve survey list
    * forEach survey retrieve detail of the survey
    * with survey detail, generate question_id:question
    * based on question_id:question and question:key generate final json

    finished already

## 2. initial / delete webhooks on surveymonkey
    goal 1: when call /deleteAllWebhooks, it will remove all webhooks on surveyMonkey survey
    goal 2: when call /initWebhooks, it will only set new webhooks when there is no webhook for current survey
    
    finished already
    
## 3. parse input json
    goal: when receive notification from webhhoks, retieve json data from survey monkey, parse it, store it in db
    
    * retrieve survey response
    * determine which survey this response for
    * choose correct idTitile file in /data/survey
    * choose correct question dictionary from /data/quesDic
    * call parseAns ans store --> get json data
    * call createStu/Staff other to insert data into db