var yaml = require('js-yaml');
var fs = require('fs');
var path = require('path');
const config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, './config.yaml'), 'utf8'));

var promise = require('bluebird');
var options = { promiseLib: promise };
var validator = require('validator');
var pgp = require('pg-promise')(options);
var db = pgp(config['POSTGRE']['URI']);

const uuidv4 = require('uuid/v4');
const uuidv5 = require('uuid/v5');
const UUIDSEED = config['UUID']['NAMESPACE'];

module.exports = {
    insertStudent:createStudent,
    insertStaff:createStaff,
}

function createStudent(input) {
    /**behaviour: student biographical infotmation should existed in req.body
     * method: POST
     * note: insert record into student/relation tables
     *       this method will be called by survery parser when we receive input data
     * */
        // console.log(req.body);
    var parseData = standardizeIn(input,'student');
    console.log('parseData: ',parseData);
    db.none('insert into student (id,create_date,bio,contact,hours,happi,badges,gpa,wma,wyz,vom)'+
        'values (${id},${create_date},${bio},${contact},${hours},${happi},${badges},${gpa},${wma},${wyz},${vom})',parseData
    ).then(function () {
        db.none('insert into relation (id,stud) values' +
            '(${id},${stud})',{'id':parseData.id,'stud':parseData.contact}
        ).then(function () {
            console.log('insert student success');
        }).catch(function (err) {
            console.log('err in relation insert',err);
        })
    }).catch(function (err) {
        console.log('err in insert into student: ',err);
    })
}

function createStaff(req,res,next) {
    /**behaviour: staff biographical information shoud existed in req.body
     * method: POST
     * note: only insert into staff table
     *       this method will be called by survery parser when we receive input data
     * */
    // console.log(req.body);
    db.none('insert into staff (id,create_date,bio,contact,relation_id,role)'+
        'values (${id},${create_date},${bio},${contact},${relation_id},${role})',standardizeIn(req.body,'staff')
    ).then(function () {
        res.status(200)
            .json({
                status: 'success',
                message: 'Insert one Staff Bio'
            });
    }).catch(function (err) {
        console.log(err);
        return next(err);
    })
}

/**tool functions to parse input data
 * */
function standardizeIn(body,mode) {
    if (!body) {
        let error = new Error('no input to standardize');
        console.log(error);
        return error;
    };
    var out = {};
    var date = new Date();
    if (mode === 'student') {
        out['id'] = uuidv5(body.email, UUIDSEED);
        out['create_date'] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        out['contact'] = getcontactString(body, mode);
        out['bio'] = JSON.stringify(body);
        out['hours'] = '{0,0,0,0,0}';
        out['happi'] = '{0,0,0,0,0}';
        out['badges'] = '{"c1"}';
        out['gpa'] = '{"GPA_website":"","GPA_password":"","GPA_username":""}';
        out['wma'] = '{"WMA_day":"","WMA_time":"","WMA_location":""}';
        out['vom'] = '{"VoM_username":"","VoM_password":""}';
        out['wyz'] = '{"WyZ_username":"","WyZ_password":""}';
    }
    else if (mode === 'staff') {
        out['id'] = uuidv5(body.email, UUIDSEED);
        out['create_date'] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        out['contact'] = getcontactString(body, mode);
        out['bio'] = JSON.stringify(body);
        out['relation_id'] = '{}';
    }
    return out;
}

function getcontactString(body,mode) {
    var out = {};
    out['id'] = uuidv5(body.email, UUIDSEED);
    out['phone'] = body.phone;
    out['email'] = body.email;
    out['wechat'] = body.wechat;
    out['skype'] = body.skype;
    out['name'] = body.family_name+' '+body.given_name;
    delete body.phone;
    delete body.email;
    delete body.wechat;
    delete body.skype;
    delete body.family_name;
    delete body.given_name;
    return JSON.stringify(out);
}
