/**
 public accessable function. basicly, most of the functions in this section are utility functions.
 1. They should never have the ability to update db or chaneg server status
 2. public accessable @public
 */
var titleParse = function(str) {
    var DEFAULT_REPLACE = /(\s)|(\/)/gi;
    if (!str || typeof(str) != 'string') {
        return null;
    }
    return str.replace(DEFAULT_REPLACE,'_');
}

var dfsQueJson = function(json,result) {
    //gnerate key-val pair of question--questions_id
    if (!json) {
        return;
    } else if (json.headings) {
        result[json.id] = json.headings[0]['heading'];
    } else if (json.text) {
        result[json.id] = json.text;
        return;
    } else if (json.title) {
        result[json.id] = json.title;
    }
    for (var i in json) {
        if (typeof(json[i]) == 'object') {
            dfsQueJson(json[i],result);
        }
    }
}

var dfsAnsJson = function(ans,question,dic,output) {
    //generate key-value pair for question and ans
    if (!ans) {
        return;
    } else if (ans.answers) {
        dealAnswerandID(ans,question,dic,question[ans.id],output);
        return;
    }
    for (var i in ans) {
        if (typeof(ans[i]) == 'object') {
            dfsAnsJson(ans[i],question,dic,output);
        }
    }
}

var dealAnswerandID = function(ans,idQues,quesKey,header,output) {
    if (typeof(ans) != 'object') {
        return;
    }
    if (ans.choice_id || ans.row_id || ans.text) {
        var key = header;
        var val = "";
        if (ans.choice_id && !ans.row_id && ! ans.text) {
            val += idQues[ans.choice_id];
        } else if (ans.choice_id && (ans.row_id || ans.text)) {
            key += idQues[ans.choice_id];
            if (ans.row_id) {
                val += idQues[ans.row_id];
            }
            if (ans.text) {
                val += ans.text;
            }
        } else if (ans.row_id && ans.text) {
            key += idQues[ans.row_id];
            val += ans.text;
        } else if (ans.row && !ans.text) {
            val += idQues[ans.row_id];
        } else {
            val += ans.text;
        }
        if (!output[quesKey[key]]) {
            output[quesKey[key]] = val;
        } else if (typeof(output[quesKey[key]]) == 'string') {
            output[quesKey[key]] = [output[quesKey[key]],val];
        } else {
            output[quesKey[key]].push(val);
        }
        return;
    } else {
        for (var cell in ans) {
            if (typeof(ans[cell]) == 'object') {
                dealAnswerandID(ans[cell],idQues,quesKey,header,output);
            }
        }
    }

}


module.exports = {
    parseAns:dfsAnsJson,
    parseTitle:titleParse,
    parseQues:dfsQueJson
}