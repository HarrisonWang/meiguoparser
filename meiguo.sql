

/*
 Navicat Premium Data Transfer

 Source Server         : MeiguoEdu_pg
 Source Server Type    : PostgreSQL
 Source Server Version : 90603
 Source Host           : localhost:5432
 Source Catalog        : meiguoedu
 Source Schema         : meiguo

 Target Server Type    : PostgreSQL
 Target Server Version : 90603
 File Encoding         : 65001

 Date: 09/08/2017 18:24:14
*/


-- ----------------------------
-- Sequence structure for login_isVisited_seq
-- ----------------------------
-- DROP SEQUENCE IF EXISTS "login_isVisited_seq";
-- CREATE SEQUENCE "login_isVisited_seq"
-- INCREMENT 1
-- MINVALUE  1
-- MAXVALUE 9223372036854775807
--  START 1
-- CACHE 1;
-- SELECT setval('"login_isVisited_seq"', 1, false);

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS "activity";
CREATE TABLE "activity" (
  "id" uuid NOT NULL DEFAULT NULL,
  "create_date" date NOT NULL DEFAULT NULL,
  "type" varchar COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "hours" int2 NOT NULL DEFAULT NULL,
  "happi" int2 NOT NULL DEFAULT NULL,
  "detail" jsonb NOT NULL DEFAULT NULL,
  "relation_id" uuid NOT NULL DEFAULT NULL,
  "tier" varchar COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "meet_date" date NOT NULL DEFAULT NULL
)
;



-- ----------------------------
-- Table structure for todo
-- ----------------------------
DROP TABLE IF EXISTS "todo";
CREATE TABLE "todo" (
  "id" uuid NOT NULL DEFAULT NULL,
  "create_date" date NOT NULL DEFAULT NULL,
  "due_date" date NOT NULL DEFAULT NULL,
  "complete" bool NOT NULL DEFAULT NULL,
  "relation_id" uuid NOT NULL DEFAULT NULL,
  "role" varchar COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "content" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL
)
;
-- ALTER TABLE "todo" OWNER TO "wjm-harry";




-- ----------------------------
-- Table structure for relation
-- ----------------------------
DROP TABLE IF EXISTS "relation";
CREATE TABLE "relation" (
  "id" uuid NOT NULL DEFAULT NULL,
  "doe" jsonb DEFAULT NULL,
  "pmc" jsonb DEFAULT NULL,
  "ft" jsonb DEFAULT NULL,
  "stud" jsonb NOT NULL DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS "staff";
CREATE TABLE "staff" (
  "id" uuid NOT NULL DEFAULT NULL,
  "create_date" date NOT NULL DEFAULT NULL,
  "bio" jsonb NOT NULL DEFAULT NULL,
  "contact" jsonb NOT NULL DEFAULT NULL,
  "relation_id" uuid[] NOT NULL DEFAULT NULL
)
;
-- ALTER TABLE "staff" OWNER TO "wjm-harry";
-- COMMENT ON COLUMN "staff"."relation_id" IS 'length can be 0';

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS "student";
CREATE TABLE "student" (
  "id" uuid NOT NULL DEFAULT NULL,
  "create_date" date NOT NULL DEFAULT NULL,
  "bio" jsonb NOT NULL DEFAULT NULL,
  "contact" jsonb DEFAULT NULL,
  "hours" integer[] NOT NULL DEFAULT NULL,
  "happi" integer[] NOT NULL DEFAULT NULL,
  "badges" text[] COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "gpa" jsonb DEFAULT NULL,
  "wma" jsonb DEFAULT NULL,
  "wyz" jsonb DEFAULT NULL,
  "vom" jsonb DEFAULT NULL
)
;
-- ALTER TABLE "student" OWNER TO "wjm-harry";

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS "login";
CREATE TABLE "login" (
  "id" uuid NOT NULL DEFAULT NULL,
  "username" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "role" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "password" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "isvisited" int2 NOT NULL DEFAULT 0,
  "nickname" varchar(16) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;
-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
-- ALTER SEQUENCE "login_isVisited_seq"
-- OWNED BY "login"."isvisited";
-- SELECT setval('"login_isVisited_seq"', 2, false);

-- ----------------------------
-- Primary Key structure for table activity
-- ----------------------------
ALTER TABLE "activity" ADD CONSTRAINT "activity_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table login
-- ----------------------------
ALTER TABLE "login" ADD CONSTRAINT "login_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table relation
-- ----------------------------
ALTER TABLE "relation" ADD CONSTRAINT "relation_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table staff
-- ----------------------------
ALTER TABLE "staff" ADD CONSTRAINT "staff_contact" UNIQUE ("contact");
COMMENT ON CONSTRAINT "staff_contact" ON "staff" IS 'this jsonb should be unique';

-- ----------------------------
-- Primary Key structure for table staff
-- ----------------------------
ALTER TABLE "staff" ADD CONSTRAINT "staff_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table student
-- ----------------------------
ALTER TABLE "student" ADD CONSTRAINT "contact" UNIQUE ("contact");
COMMENT ON CONSTRAINT "contact" ON "student" IS 'this jsonb should be unique
';

-- ----------------------------
-- Primary Key structure for table student
-- ----------------------------
ALTER TABLE "student" ADD CONSTRAINT "student_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table todo
-- ----------------------------
ALTER TABLE "todo" ADD CONSTRAINT "todo_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table activity
-- ----------------------------
ALTER TABLE "activity" ADD CONSTRAINT "r_id" FOREIGN KEY ("relation_id") REFERENCES "relation" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table relation
-- ----------------------------
ALTER TABLE "relation" ADD CONSTRAINT "relation_doe" FOREIGN KEY ("doe") REFERENCES "staff" ("contact") ON DELETE SET NULL ON UPDATE RESTRICT;
ALTER TABLE "relation" ADD CONSTRAINT "relation_ft" FOREIGN KEY ("ft") REFERENCES "staff" ("contact") ON DELETE SET NULL ON UPDATE RESTRICT;
ALTER TABLE "relation" ADD CONSTRAINT "relation_pmc" FOREIGN KEY ("pmc") REFERENCES "staff" ("contact") ON DELETE SET NULL ON UPDATE RESTRICT;
ALTER TABLE "relation" ADD CONSTRAINT "relation_stu" FOREIGN KEY ("stud") REFERENCES "student" ("contact") ON DELETE CASCADE ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table staff
-- ----------------------------
ALTER TABLE "staff" ADD CONSTRAINT "primary" FOREIGN KEY ("id") REFERENCES "login" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table student
-- ----------------------------
ALTER TABLE "student" ADD CONSTRAINT "primary" FOREIGN KEY ("id") REFERENCES "login" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table todo
-- ----------------------------
ALTER TABLE "todo" ADD CONSTRAINT "r_id" FOREIGN KEY ("relation_id") REFERENCES "relation" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;



-- ----------------------------
-- Insert Sample Data
-- ----------------------------
-- ----------------------------
-- Records of login
-- ----------------------------
BEGIN;
INSERT INTO "login" VALUES ('5f7ef216afee0d20af4d5f333c0a9df8', 'wjm@me.com', 'DOE', '$2a$10$VgOu3gW5Z0k1exdQCM1VgOa72Ns0Alig0rS13qoPaNdo0MTcK8fN.', 0, 'wjm');
INSERT INTO "login" VALUES ('4a4c84a9-160d-2c56-4199-8c6ac9b19548', 'vp@me.com', 'ADMIN', '$2a$10$U5PEMDMoGm7.CsQZxDhN6e7m9fmDwbrZx3Splxx5bJOtNuaEXo82m', 22, 'Veronica');
INSERT INTO "login" VALUES ('ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', 'ds@me.com', 'STUD', '$2a$10$9Cuf/Dc6NS0vcZEql3SCqOjv3L1kKHkn.Ue0xMEG4RjrToF.2EDUC', 1, 'Desire');
INSERT INTO "login" VALUES ('46592467-7006-cecf-94ea-05bc11690ab8', 'siyuan@me.com', 'STUD', '$2a$10$VAsXZ1Vaa1tt1fkA/rWA1.IENY.BDBpLpW3F0cXu4xeOrJtJnNwRS', 1, 'c1');
INSERT INTO "login" VALUES ('a2bce774-7224-1256-4354-facb93c3cf83', 'w@me.com', 'PMC', '$2a$10$yfIAFCK6/nUTIVaWYCtU7Oi9Neng01p1QXuZNMPFaWmDfBUzsamD2', 1, 'WJM_PMC');
INSERT INTO "login" VALUES ('ca55a39a-d447-4191-3e08-32e45e79d257', 'j@me.com', 'FT', '$2a$10$5NuXnqfOPJHiai46zS2DU.OkynMJ3O2MIvAuPI8GEecsDnJeXJnEq', 1, 'WJM_FT');
INSERT INTO "login" VALUES ('34e5c3ff-f867-5732-a9c2-09e22a1c6537', 'huangjintianyue@gmail.com', 'STUD', '$2a$10$yiMx.2SPHtMiVWR/sCCmteXIkcpMc8FIIG8EjfekDDwHMhkp1F2Tq', 1, 'chris');
COMMIT;

INSERT INTO "staff" VALUES (
  '4a4c84a9-160d-2c56-4199-8c6ac9b19548','2017-07-27',
  '{"family_name":"veronica","given_name":"proud","preferred_name":"veronica","gender":"Female","birthday":"1990-05-09","phone":"324432432423","wechat":"jinxiangcao","skype":"veronica","religion":"none","role":"ADMIN","photo":"./img/4a8a08f09d37b73795649038408b5f33.png","email":"veronica@meiguola.edu"}',
  '{"id":"4a4c84a9-160d-2c56-4199-8c6ac9b19548","email":"veronica@meiguola.com","phone":"324432432423","wechat":"jinxiangcao","name":"veronica prout"}','{}');

INSERT INTO "staff" VALUES (
  '5f7ef216afee0d20af4d5f333c0a9df8','2017-07-27',
  '{"family_name":"wang","given_name":"jiameng","preferred_name":"harrison","gender":"Male","birthday":"1993-07-01","phone":"2132967823","wechat":"jiamneng_wang","skype":"fdsads","religion":"none","role":"DOE","photo":"./img/4a8a08f09d37b73795649038408b5f33.png","email":"wangharry71@yahoo.com"}',
  '{"id":"5f7ef216afee0d20af4d5f333c0a9df8","email":"wjm@me.com","phone":"2132145434","wechat":"meiguola","name":"jiameng wang"}','{ffaff937-53e0-bfb5-0b56-aa0d83d9b1be}');

INSERT INTO "staff" VALUES (
  'a2bce774-7224-1256-4354-facb93c3cf83','2017-07-27',
  '{"family_name":"PMC1","given_name":"jiameng","preferred_name":"harrison","gender":"Male","birthday":"1993-07-01","phone":"2132967823","wechat":"jiamneng_wang","skype":"wjm_pmc","religion":"none","role":"PMC","photo":"./img/4a8a08f09d37b73795649038408b5f33.png","email":"wangharry71@yahoo.com"}',
  '{"id":"a2bce774-7224-1256-4354-facb93c3cf83","email":"w@me.com","phone":"2132145434","wechat":"meiguola","name":"PMC1 jiameng"}','{ffaff937-53e0-bfb5-0b56-aa0d83d9b1be}');

INSERT INTO "staff" VALUES (
  'ca55a39a-d447-4191-3e08-32e45e79d257','2017-07-27',
  '{"family_name":"FT1","given_name":"Wang","preferred_name":"harrison","gender":"Male","birthday":"1993-07-01","phone":"24322967823","wechat":"jiamneng_wang","skype":"fdsads","religion":"none","role":"FT","photo":"./img/4a8a08f09d37b73795649038408b5f33.png","email":"wangharry71@yahoo.com"}',
  '{"id":"ca55a39a-d447-4191-3e08-32e45e79d257","email":"j@me.com","phone":"2132145434","wechat":"meiguola","name":"FT1 Wang"}','{ffaff937-53e0-bfb5-0b56-aa0d83d9b1be}');


INSERT INTO "student" VALUES(
  'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be',
  '2017-07-27',
  '{"skype": "desiredesire", "gender": "Female", "birthday": "1/1/1993", "given_name": "Sayarath", "family_name": "desire", "chinese_name": "茉莉", "english_name": "desire", "preferred_name": "desire", "family_bio_siblings": "No", "school_chinese_name": "desire", "phone":"21342145435", "email":"desire@meiguola.com","wechat":"desireesayarath","religion":"none","street":"830 Traction Ave","city":"Los Angeles","district":"CA","zipcode":"90013","country":"USA","street":"830 Traction Ave","city":"Los Angeles","district":"CA","zipcode":"90013","country":"USA","home_street":"830 Traction Ave","home_city":"Los Angeles","home_state":"CA","home_zipcode":"90013","home_country":"USA","primary_language":"english","school_english_name": "wjm", "extracurricular_work": "wjm", "school_chinese_pinyin": "desire", "extracurricular_volunteer": "desire", "extracurricular_education_reason": "Get a job"}',
  '{"id":"ffaff937-53e0-bfb5-0b56-aa0d83d9b1be","email":"ds@me.com","phone":"2132145434","wechat":"meiguola","name":"harrison wang"}',
  '{1,2,3,8,0}',
  '{2,3,4,6,8}',
  '{"b1","c4"}',
  '{"GPA_website":"whwh","GPA_username":"hdgfjh","GPA_password":"hjkh"}',
  '{"WMA_day":"","WMA_time":"","WMA_location":""}',
  '{"WyZ_username":"","WyZ_password":""}',
  '{"VoM_username":"","VoM_password":""}'
);

INSERT INTO "relation" ("id", "doe", "pmc", "ft", "stud") VALUES ('ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', '{"id":"5f7ef216afee0d20af4d5f333c0a9df8","email":"wjm@me.com","phone":"2132145434","wechat":"meiguola","name":"jiameng wang"}', '{"id":"a2bce774-7224-1256-4354-facb93c3cf83","email":"w@me.com","phone":"2132145434","wechat":"meiguola","name":"PMC1 jiameng"}', '{"id":"ca55a39a-d447-4191-3e08-32e45e79d257","email":"j@me.com","phone":"2132145434","wechat":"meiguola","name":"FT1 Wang"}','{"id":"ffaff937-53e0-bfb5-0b56-aa0d83d9b1be","email":"ds@me.com","phone":"2132145434","wechat":"meiguola","name":"harrison wang"}');

INSERT INTO "todo" VALUES(
  'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be',
  '2017-07-11',
  '2017-07-11',
  'true',
  'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be',
  'STUD',
  'dsfsdfdsfdsf'
);


INSERT INTO "todo" VALUES(
  'ffaff937-53e0-bfb5-0b56-aa0d88d9b2be',
  '2017-07-01',
  '2017-07-11',
  'true',
  'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be',
  'PMC',
  'this is todo for pmc'
);


INSERT INTO "activity" VALUES(
  'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be',
  '2017-07-01',
  'SKILL',
  '2',
  '5',
  '{"name":"dsf","position":"afds","describe":"fdgfdsg"}',
  'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be',
  '1',
  '2017-09-01'
);

INSERT INTO "activity" VALUES(
  'ffaff937-53e0-bfb5-0b56-aa0d83d8b1be',
  '2017-08-01',
  'WORK',
  '0',
  '1',
  '{"name":"adfsd","position":"dafds","describe":"dsaf"}',
  'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be',
  '1',
  '2017-07-01'
);
